import Fastify from "fastify";
import amqplib, { Channel } from "amqplib";

(async () => {
  const queue = "myqueue";
  const connection = await amqplib.connect("amqp://localhost:5672");
  const channel = await connection.createChannel();
  try {
    await channel.assertQueue(queue);
  } catch (err) {
    console.log(err);
  }

  const fastify = Fastify({
    logger: true,
  });

  fastify.get("/", async (request, reply) => {
    const asd = await channel.get(queue);
    if (!asd) {
      reply.send("No new message");
      return;
    }
    
    channel.ack(asd);
    reply.send(JSON.parse(asd.content.toString()));
    });

  fastify.listen({ port: 3002 }, function (err, address) {
    if (err) {
      fastify.log.error(err);
      process.exit(1);
    }
    console.log(`Server is now listening on ${address}`);
  });
})();
