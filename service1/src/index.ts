import Fastify from "fastify";
import amqplib, { Channel } from "amqplib";
let connection: amqplib.Connection, channel: amqplib.Channel;

(async () => {
  const queue = "myqueue";
  try {
    connection = await amqplib.connect("amqp://localhost:5672");
    channel = await connection.createChannel();
    await channel.assertQueue(queue);
  } catch (err) {
    console.log(err);
  }
  const fastify = Fastify({
    logger: true,
  });

  fastify.post("/send", async function (request, reply) {
    const fakeData = {
      name: "Elon Musk",
      company: "SpaceX",
    };

    await channel.sendToQueue(queue, Buffer.from(JSON.stringify(request.body)));
    reply.send(request.body);
  });

  fastify.listen({ port: 3001 }, function (err, address) {
    if (err) {
      fastify.log.error(err);
      process.exit(1);
    }
    console.log(`Server is now listening on ${address}`);
  });
})();
